from pathlib import Path
from typing import Any, Dict, List, Union

import numpy as np

from ...abc import Parser


class InputParser(Parser):
    """CP2K Input Parser."""

    @classmethod
    def parse(cls, inpath) -> Dict[str, Any]:
        """Parse input."""
        result: Dict[str, Any] = {}
        path = Path(inpath)
        with open(path, 'rt') as f:
            lines = f.readlines()

        result['input_file'] = str(inpath)

        for i, line in enumerate(lines):
            if '&CELL' in line:
                raw_cell = []
                order = []
                for j in range(i + 1, i + 4):
                    line_split = lines[j].split()
                    order.append(line_split[0])
                    vals = [float(line_split[k]) for k in range(1, 4)]
                    raw_cell.append(np.array(vals))

                result['unit_cell'] = np.array(
                    [x for _, x in sorted(zip(order, raw_cell))])

            if '&COORD' in line:
                xyz: List[Union[List[float], float]] = []
                atoms: List[str] = []
                for j in range(i + 1, len(lines)):
                    if '&END COORD' in lines[j]:
                        break

                    line_split = lines[j].split()
                    atoms.append(line_split[0])
                    vals = [float(line_split[k]) for k in range(1, 4)]
                    xyz.append(np.array(vals))

                result['atoms'] = atoms
                result['coordinates'] = np.array(xyz,
                                                 dtype=np.float64).ravel()

        return result
